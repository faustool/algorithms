package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public interface BinaryNode<T> extends Node<T> {

    BinaryNode<T> getLeft();

    void setLeft(BinaryNode<T> left);

    BinaryNode<T> getRight();

    void setRight(BinaryNode<T> right);

    boolean hasLeft();

    boolean hasRight();

    boolean isComplete();

    boolean onlyMissRight();

    BinaryNode<T> getParent();

    void setParent(BinaryNode<T> node);

}
