package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public interface Node<T> {
    T getValue();

    void setValue(T value);
}
