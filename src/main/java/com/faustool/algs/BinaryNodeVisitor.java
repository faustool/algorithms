package com.faustool.algs;

import java.util.function.Consumer;

/**
 * Created by fausto on 22/01/2017.
 */
public interface BinaryNodeVisitor<T> {

    void visit(BinaryNode<T> binaryNode, Consumer<T> visitor);

}
