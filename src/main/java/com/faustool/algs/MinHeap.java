package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public class MinHeap<T extends Comparable> implements Heap<T> {

    private BinaryNode<T> root;

    @Override
    public void put(BinaryNode<T> node) {
        if (node == null) {
            throw new NullPointerException("Cannot push a null node");
        }
        if (root == null) {
            setRoot(node);
        } else {
            pushToEnd(node, root);
            bubbleUp(node);
        }
        System.out.println(root);
    }

    @Override
    public BinaryNode<T> getRoot() {
        return root;
    }

    private void pushToEnd(BinaryNode<T> node, BinaryNode<T> target) {
        if (target.isComplete()) {
            if (target.getLeft().isComplete())
                pushToEnd(node, target.getRight());
            else
                pushToEnd(node, target.getLeft());
        } else {
            if (!target.hasLeft())
                target.setLeft(node);
            else if (!target.hasRight())
                target.setRight(node);
            else
                pushToEnd(node, target.getRight());
        }
    }

    protected void bubbleUp(BinaryNode<T> goingUp) {
        if (shouldGoUp(goingUp)) {
            swap(goingUp.getParent(), goingUp);
            bubbleUp(goingUp);
        }
    }

    private boolean shouldGoUp(BinaryNode<T> candidate) {
        BinaryNode<T> candidateParent = candidate.getParent();
        return candidateParent != null && candidateParent.getValue().compareTo(candidate.getValue()) > 0;
    }

    @Override
    public BinaryNode<T> pop() {
        if (this.root != null) {
            BinaryNode<T> currentRoot = this.root;
            if (!root.hasLeft() && !root.hasRight()) {
                setRoot(null);
            } else {
                BinaryNode<T> bottom = extractBottom(this.root);
                setRoot(bottom);
                pushDown(root);
            }
            return currentRoot;
        } else {
            return null;
        }
    }

    private void setRoot(BinaryNode<T> newRoot) {
        if (newRoot != null) {
            if (root != null) {
                BinaryNode<T> currentRootLeft = this.root.getLeft();
                BinaryNode<T> currentRootRight = this.root.getRight();

                newRoot.setLeft(currentRootLeft);
                newRoot.setRight(currentRootRight);
            }
            newRoot.setParent(null);
        }
        root = newRoot;
    }

    private void pushDown(BinaryNode<T> goingDown) {
        if (goingDown.isComplete()) {
            if (shouldGoDownLeftOverRight(goingDown)) {
                swap(goingDown, goingDown.getLeft());
            } else {
                swap(goingDown, goingDown.getRight());
            }
            pushDown(goingDown);
        } else if (shouldGoDownRight(goingDown)) {
            swap(goingDown, goingDown.getRight());
            pushDown(goingDown);
        } else if (shouldGoDownLeft(goingDown)) {
            swap(goingDown, goingDown.getLeft());
            pushDown(goingDown);
        }
    }

    private boolean shouldGoDownLeftOverRight(BinaryNode<T> goingDown) {
        return goingDown.getLeft().getValue().compareTo(goingDown.getRight().getValue()) < 0;
    }

    private boolean shouldGoDownLeft(BinaryNode<T> goingDown) {
        return goingDown.hasLeft() && goingDown.getLeft().getValue().compareTo(goingDown.getValue()) < 0;
    }

    private boolean shouldGoDownRight(BinaryNode<T> goingDown) {
        return goingDown.hasRight() && goingDown.getRight().getValue().compareTo(goingDown.getValue()) < 0;
    }

    private void swap(BinaryNode<T> goingDown, BinaryNode<T> goingUp) {
        BinaryNode<T> goingDownLeft = goingDown.getLeft();
        BinaryNode<T> goingDownRight = goingDown.getRight();
        BinaryNode<T> goingDownParent = goingDown.getParent();

        BinaryNode<T> goingUpLeft = goingUp.getLeft();
        BinaryNode<T> goingUpRight = goingUp.getRight();

        goingDown.setLeft(goingUpLeft);
        goingDown.setRight(goingUpRight);

        if (goingUp.equals(goingDownLeft)) {
            goingUp.setLeft(goingDown);
            goingUp.setRight(goingDownRight);
        } else {
            goingUp.setRight(goingDown);
            goingUp.setLeft(goingDownLeft);
        }

        if (goingDownParent != null) {
            if (goingDownParent.hasRight() && goingDownParent.getRight().equals(goingDown))
                goingDownParent.setRight(goingUp);
            else
                goingDownParent.setLeft(goingUp);
        } else {
            root = goingUp;
            goingUp.setParent(null);
        }

    }

    protected BinaryNode<T> extractBottom(BinaryNode<T> node) {
        if (node.hasRight())
            return extractBottom(node.getRight());
        else if (node.hasLeft())
            return extractBottom(node.getLeft());
        else {
            if (node.getParent() != null) {
                BinaryNode<T> nodeParent = node.getParent();
                if (nodeParent.hasLeft() && nodeParent.getLeft().equals(node))
                    nodeParent.setLeft(null);
                else if (nodeParent.hasRight() && nodeParent.getRight().equals(node))
                    nodeParent.setRight(null);
            }
            node.setParent(null);
            return node;
        }
    }

    @Override
    public BinaryNode<T> peek() {
        return root;
    }
}
