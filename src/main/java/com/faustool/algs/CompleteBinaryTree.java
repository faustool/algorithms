package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public class CompleteBinaryTree<T> implements BinaryTree<T> {

    private BinaryNode<T> root;

    @Override
    public BinaryNode<T> getRoot() {
        return root;
    }

    @Override
    public void put(BinaryNode<T> node) {
        if (node == null)
            throw new NullPointerException("Cannot add a null node");
        if (root == null) {
            root = node;
        } else {
            put(node, root);
        }
    }

    protected void put(BinaryNode<T> node, BinaryNode<T> target) {
        if (target.isComplete()) {
            if (target.getLeft().isComplete())
                put(node, target.getRight());
            else
                put(node, target.getLeft());
        } else if (target.onlyMissRight()) {
            target.setRight(node);
        } else {
            target.setLeft(node);
        }
    }

}
