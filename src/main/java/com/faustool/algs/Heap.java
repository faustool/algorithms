package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public interface Heap<T extends Comparable> extends BinaryTree<T>{

    BinaryNode<T> pop();

    BinaryNode<T> peek();

}
