package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public interface BinaryTree<T> {

    void put(BinaryNode<T> node);

    BinaryNode<T> getRoot();
}
