package com.faustool.algs;

import java.util.function.Consumer;

/**
 * Created by fausto on 22/01/2017.
 */
public class InOrderBinaryNodeVisitor<T> implements BinaryNodeVisitor<T> {
    @Override
    public void visit(BinaryNode<T> binaryNode, Consumer<T> visitor) {
        if (binaryNode == null)
            return;
        visit(binaryNode.getLeft(), visitor);
        visitor.accept(binaryNode.getValue());
        visit(binaryNode.getRight(), visitor);
    }
}
