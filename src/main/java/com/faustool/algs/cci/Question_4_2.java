package com.faustool.algs.cci;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.util.Arrays;

import static java.util.Arrays.copyOfRange;

/**
 * <p><b>Minimal Tree:</b> Given a sorted (increasing order) array with unique integer elements, write an algorithm
 * to create a binary search tree with minimal isBalanced.</p>
 */
public class Question_4_2 {

    static class Node {
        Integer value;
        Node right;
        Node left;

        Node(int value) {
            this.value = value;
        }

        Node(int value, Node left, Node right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    public Node createTree(int... array) {
        if (array.length == 0)
            return null;
        if (array.length == 1)
            return new Node(array[0]);
        if (array.length == 2)
            return new Node(array[1], new Node(array[0]), null);
        else if (array.length == 3)
            return new Node(array[1], new Node(array[0]), new Node(array[2]));
        else {
            int halfIndex = array.length / 2;
            int halfValue = array[halfIndex];
            return new Node(halfValue,
                    createTree(copyOfRange(array, 0, halfIndex)), // left
                    createTree(copyOfRange(array, halfIndex + 1, array.length)) // right
            );
        }
    }

}
