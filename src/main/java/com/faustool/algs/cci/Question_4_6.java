package com.faustool.algs.cci;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_6 {

    static class Node {
        Integer value;
        Node right;
        Node left;
        Node parent;

        Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }


    public Node getNextInOrder(Node node) {
        if (node == null) {
            return null;
        } else if (node.right != null) {
            node = node.right;
            Node left;
            while ((left = node.left) != null) {
                node = left;
            }
            return node;
        } else { // node is at bottom
            Node parent;
            while ((parent = node.parent) != null) {
                if (parent.value >= node.value) // Node is at left, next is parent
                    return parent;
                else { // node is at right, next is above
                    node = parent;
                }
            }
            return null;
        }
    }

}
