package com.faustool.algs.cci;

import java.util.*;

/**
 * <p><b>Validate BST:</b> Implement a function to check if a binary tree is a binary search tree.</p>
 */
public class Question_4_5 {

    static class Node {
        Integer value;
        int depth = 0;
        Node right;
        Node left;

        Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    public boolean isBinarySearchTree(Node tree) {
        Queue<Node> queue = new LinkedList<>();
        Map<Integer, Integer> depthLastValue = new HashMap<>();
        queue.add(tree);
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (depthLastValue.containsKey(node.depth)) {
                if (depthLastValue.get(node.depth) >= node.value) // left nodes in depth must always be
                    return false;                                  // smaller than right nodes in depth
            }
            depthLastValue.put(node.depth, node.value);

            if (node.left != null) {
                if (node.left.value > node.value) // left values must always be smaller or equal to parent's
                    return false;
                node.left.depth = node.depth + 1;
                queue.add(node.left);
            }
            if (node.right != null) {
                if (node.right.value <= node.value) // right values must always be greater than parent's
                    return false;
                node.right.depth = node.depth + 1;
                queue.add(node.right);
            }
        }
        return true;
    }

}
