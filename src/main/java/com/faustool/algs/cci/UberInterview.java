package com.faustool.algs.cci;

import java.util.Arrays;

/**
 * Created by fausto on 06/02/2017.
 */
public class UberInterview {

    public static void main(String[] args) {

        String[] inputs = {"CBACBACBACBACBACBACBA", "AAAAABBBBBCCCC", "CCCBBBAAA", "ABCABCABCABCABCABCABC"};

        for (String input : inputs) {
            char[] data = input.toCharArray();

            int a = 0;
            int c = data.length - 1;

            int changes;
            int loops = 0;
            do {
                changes = 0;
                while (data[a] == 'A')
                    a++;
                while (data[c] == 'C')
                    c--;
                for (int i = a; i <= c; i++) {
                    loops++;
                    if (data[i] == 'A') {
                        char moving = data[a];
                        data[a++] = data[i];
                        data[i] = moving;
                        changes++;
                    }
                    if (data[i] == 'C') {
                        char moving = data[c];
                        data[c--] = data[i];
                        data[i] = moving;
                        changes++;
                    }
                }
            } while (changes > 0);
            System.out.printf("Sorted array of size %d in %d iterations: %s\n"
                    , data.length, loops, Arrays.toString(data));
        }

    }


}
