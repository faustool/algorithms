package com.faustool.algs.cci;

import java.util.*;

/**
 * <p><b>Check Balanced:</b> Implement a function to check if a binary tree is balanced. For the purpose of this
 * question, a balanced tree is defined to be a tree such that the heights of the two subtrees of any node never differ
 * by more than one.</p>
 */
public class Question_4_4 {

    static class Node {
        Integer value;
        int height = 0;
        Node right;
        Node left;

        Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    static class UnbalancedTreeException extends Exception {
    }

    public boolean isBalanced(Node tree) throws UnbalancedTreeException {
        Queue<Node> queue = new LinkedList<>();
        List<List<Node>> listOfNodesInDepth = new ArrayList<>();
        queue.add(tree);
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            final List<Node> nodesInDepth;
            if (listOfNodesInDepth.size() > node.height) {
                nodesInDepth = listOfNodesInDepth.get(node.height);
            } else {
                if (node.height > 1) {
                    int previousHight = node.height - 1;
                    List<Node> previousDepth = listOfNodesInDepth.get(previousHight);
                    if (previousDepth.size() != previousHight * 2)
                        return false;
                }
                nodesInDepth = new ArrayList<>();
                listOfNodesInDepth.add(nodesInDepth);
            }
            nodesInDepth.add(node);
            if (node.left != null) {
                node.left.height = node.height + 1;
                queue.add(node.left);
            }
            if (node.right != null) {
                node.right.height = node.height + 1;
                queue.add(node.right);
            }
        }
        return true;
    }

}
