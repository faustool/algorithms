package com.faustool.algs.cci;

import java.util.*;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_7 {

    static class Node {
        final String value;

        boolean visited = false;

        Node(String value) {
            this.value = value;
        }
        List<Node> connections = new ArrayList<>();
    }

    static class Graph {
        Map<String, Node> nodes = new HashMap<>();
        Node root = new Node(null);
    }

    public List<String> getBuildOrder(String[] projects, String[] dependencies) {
        if (dependencies.length % 2 != 0) {
            throw new IllegalArgumentException("The length of dependencies array is expected to be even (pairs)");
        }

        Graph graph = new Graph();

        for (String project : projects) {
            Node node = new Node(project);
            graph.nodes.put(project, node);
            graph.root.connections.add(node);
        }

        Iterator<String> iterator = Arrays.asList(dependencies).iterator();
        while (iterator.hasNext()) {
            Node dependency = graph.nodes.get(iterator.next());
            Node dependant = graph.nodes.get(iterator.next());
            dependency.connections.add(dependant);
            graph.root.connections.remove(dependant);
        }

        if (graph.root.connections.isEmpty())
            throw new IllegalArgumentException("In this build configuration all projects depend on something and " +
                    "no order can be established");

        Queue<Node> buildQueue = new LinkedList<>();
        buildQueue.addAll(graph.root.connections);

        List<String> buildOrder = new LinkedList<>();

        while (!buildQueue.isEmpty()) {
            Node node = buildQueue.poll();
            if (!node.visited) {
                buildQueue.addAll(node.connections);
                node.visited = true;
                buildOrder.add(node.value);
            }
        }

        return buildOrder;
    }

}
