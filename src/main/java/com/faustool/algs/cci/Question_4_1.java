package com.faustool.algs.cci;

import java.util.*;

/**
 * <p><b>Route between nodes:</b> Given a directed graph, design an algorithm to find out whether there is a
 * route between two nodes</p>
 */
public class Question_4_1 {

    static class Node {
        String name = "";
        String visitingId = null;

        Set<Node> adjacentSet = new HashSet<>();

        Node(String name) {
            if (name == null)
                throw new NullPointerException("Cannot have a null name");
            this.name = name;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj || obj instanceof Node && this.name.equals(((Node) obj).name);
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public void connect(Node from, Node to) {
        from.adjacentSet.add(to);
    }

    public boolean isThereARoute(Node from, Node to) {
        if (from.equals(to))
            return true;

        String uuid = UUID.randomUUID().toString();
        Queue<Node> queue = new LinkedList<>();

        queue.add(from);
        from.visitingId = uuid;

        while (!queue.isEmpty()) {
            Node toVisit = queue.poll();
            if (to.equals(toVisit))
                return true;
            else {
                for (Node adjacent : toVisit.adjacentSet) {
                    if (!uuid.equals(adjacent.visitingId)) {
                        adjacent.visitingId = uuid;
                        queue.add(adjacent);
                    }
                }
            }
        }

        return false;
    }

}
