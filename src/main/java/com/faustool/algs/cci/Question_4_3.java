package com.faustool.algs.cci;

import java.util.ArrayList;
import java.util.List;

/**
 * <p><b>List of Depths:</b> Given a binary tree, design an algorithm which creates a linked list of all the nodes at
 * each depth (e.g., if you have a tree with depth D, you'll have D linked lists).</p>
 */
public class Question_4_3 {

    static class Node {
        Integer value;
        Node right;
        Node left;

        Node(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }
    }

    public List<List<Node>> getDepthLists(Node root) {
        List<List<Node>> result = new ArrayList<>();

        ArrayList<Node> depth0 = new ArrayList<>();
        depth0.add(root);
        result.add(depth0);

        buildResult(result, root, 1);

        return result;
    }

    private void buildResult(List<List<Node>> result, Node root, final int depth) {
        if (root != null && (root.left != null || root.right != null)) {
            final List<Node> depthNodes;
            if (result.size() > depth) {
                depthNodes = result.get(depth);
            } else {
                depthNodes = new ArrayList<>();
                result.add(depthNodes);
            }
            if (root.left != null) {
                depthNodes.add(root.left);
                buildResult(result, root.left, depth + 1);
            }
            if (root.right != null) {
                depthNodes.add(root.right);
                buildResult(result, root.right, depth + 1);
            }
        }
    }
}
