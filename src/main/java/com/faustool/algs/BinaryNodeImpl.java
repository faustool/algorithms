package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinaryNodeImpl<T> implements BinaryNode<T> {

    private T value;

    private BinaryNode<T> parent;

    private BinaryNode<T> left;

    private BinaryNode<T> right;

    public BinaryNodeImpl() {

    }

    public BinaryNodeImpl(T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public BinaryNode<T> getLeft() {
        return left;
    }

    @Override
    public void setLeft(BinaryNode<T> left) {
        if (this.equals(left))
            throw new IllegalArgumentException("Circular referencing nodes is not permitted");
        this.left = left;
        if (left != null)
            left.setParent(this);
    }

    @Override
    public BinaryNode<T> getRight() {
        return right;
    }

    @Override
    public void setRight(BinaryNode<T> right) {
        if (this.equals(right))
            throw new IllegalArgumentException("Circular referencing nodes is not permitted");
        this.right = right;
        if (right != null)
            right.setParent(this);
    }

    @Override
    public boolean hasLeft() {
        return left != null;
    }

    @Override
    public boolean hasRight() {
        return right != null;
    }

    @Override
    public boolean isComplete() {
        return hasLeft() && hasRight();
    }

    @Override
    public boolean onlyMissRight() {
        return hasLeft() && !hasRight();
    }

    @Override
    public BinaryNode<T> getParent() {
        return this.parent;
    }

    @Override
    public void setParent(BinaryNode<T> node) {
        this.parent = node;
    }

    @Override
    public String toString() {
        return getNodeValue(this);
    }

    private String getNodeValue(BinaryNode<T> node) {
        if (node != null && node.getValue() != null)
            return node.getValue().toString();
        else
            return "";
    }

}
