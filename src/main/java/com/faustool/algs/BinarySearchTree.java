package com.faustool.algs;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinarySearchTree<T extends Comparable> implements BinaryTree<T> {

    private BinaryNode<T> root;

    @Override
    public BinaryNode<T> getRoot() {
        return root;
    }

    @Override
    public void put(BinaryNode<T> node) {
        if (node == null)
            throw new NullPointerException("Cannot add a null node");
        if (node.getValue() == null)
            throw new IllegalArgumentException("Cannot add a node with null value");
        else {
            if (root == null) {
                root = node;
            } else {
                put(node, root);
            }
        }
    }

    protected void put(BinaryNode<T> node, BinaryNode<T> target) {
        int comparision = node.getValue().compareTo(target.getValue());
        if (comparision < 0) {
            if (target.hasLeft())
                put(node, target.getLeft());
            else
                target.setLeft(node);
        } else {
            if (target.hasRight())
                put(node, target.getRight());
            else
                target.setRight(node);
        }
    }
}
