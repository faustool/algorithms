package com.faustool.algs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 23/01/2017.
 */
public class MinHeapTest {

    @Test
    public void test() {
        Heap<Integer> heap = new MinHeap<>();

        heap.put(node(4));
        heap.put(node(50));
        heap.put(node(55));
        heap.put(node(90));
        heap.put(node(7));
        heap.put(node(87));
        heap.put(node(2));

        assertThat(heap.pop().getValue()).isEqualTo(2);
        assertThat(heap.pop().getValue()).isEqualTo(4);
        assertThat(heap.pop().getValue()).isEqualTo(7);
        assertThat(heap.pop().getValue()).isEqualTo(50);
        assertThat(heap.pop().getValue()).isEqualTo(55);
        assertThat(heap.pop().getValue()).isEqualTo(87);
        assertThat(heap.pop().getValue()).isEqualTo(90);
        assertThat(heap.pop()).isNull();
    }

    private BinaryNodeImpl<Integer> node(int value) {
        return new BinaryNodeImpl<>(value);
    }

}
