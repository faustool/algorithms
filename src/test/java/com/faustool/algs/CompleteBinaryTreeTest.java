package com.faustool.algs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 22/01/2017.
 */
public class CompleteBinaryTreeTest extends BinaryTreeTestBase {

    @Test
    public void testPut() {
        CompleteBinaryTree<Integer> bt = new CompleteBinaryTree<>();
        assertThat(bt.getRoot()).isNull();

        bt.put(new BinaryNodeImpl<>(10));
        assertNode(bt.getRoot(), null, 10, null);
        assertNode(bt.getRoot().getLeft(), null, null, null);
        assertNode(bt.getRoot().getRight(), null, null, null);

        bt.put(new BinaryNodeImpl<>(5));
        assertNode(bt.getRoot(), 5, 10, null);
        assertNode(bt.getRoot().getLeft(), null, 5, null);
        assertNode(bt.getRoot().getRight(), null, null, null);

        bt.put(new BinaryNodeImpl<>(15));
        assertNode(bt.getRoot(), 5, 10, 15);
        assertNode(bt.getRoot().getLeft(), null, 5, null);
        assertNode(bt.getRoot().getRight(), null, 15, null);

        bt.put(new BinaryNodeImpl<>(3));
        assertNode(bt.getRoot(), 5, 10, 15);
        assertNode(bt.getRoot().getLeft(), 3, 5, null);
        assertNode(bt.getRoot().getRight(), null, 15, null);

        bt.put(new BinaryNodeImpl<>(200));
        assertNode(bt.getRoot(), 5, 10, 15);
        assertNode(bt.getRoot().getLeft(), 3, 5, 200);
        assertNode(bt.getRoot().getRight(), null, 15, null);

        bt.put(new BinaryNodeImpl<>(50));
        assertNode(bt.getRoot(), 5, 10, 15);
        assertNode(bt.getRoot().getLeft(), 3, 5, 200);
        assertNode(bt.getRoot().getRight(), 50, 15, null);

        bt.put(new BinaryNodeImpl<>(60));
        assertNode(bt.getRoot(), 5, 10, 15);
        assertNode(bt.getRoot().getLeft(), 3, 5, 200);
        assertNode(bt.getRoot().getRight(), 50, 15, 60);
    }

    @Test(expected = NullPointerException.class)
    public void putNull() {
        CompleteBinaryTree<Integer> bst = new CompleteBinaryTree<>();
        bst.put(null);
    }

}