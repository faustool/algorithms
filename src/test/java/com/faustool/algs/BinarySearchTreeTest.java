package com.faustool.algs;

import org.junit.Test;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinarySearchTreeTest extends BinaryTreeTestBase {

    @Test
    public void put() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        assertNode(bst.getRoot(), null, null, null);

        bst.put(new BinaryNodeImpl<>(10));
        assertNode(bst.getRoot(), null, 10, null);

        bst.put(new BinaryNodeImpl<>(15));
        assertNode(bst.getRoot(), null, 10, 15);

        bst.put(new BinaryNodeImpl<>(5));
        assertNode(bst.getRoot(), 5, 10, 15);

        bst.put(new BinaryNodeImpl<>(3));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getRight(), null, 15, null);

        bst.put(new BinaryNodeImpl<>(4));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), null, 3, 4);
        assertNode(bst.getRoot().getRight(), null, 15, null);

        bst.put(new BinaryNodeImpl<>(12));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), null, 3, 4);
        assertNode(bst.getRoot().getRight(), 12, 15, null);

        bst.put(new BinaryNodeImpl<>(17));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), null, 3, 4);
        assertNode(bst.getRoot().getRight(), 12, 15, 17);

        bst.put(new BinaryNodeImpl<>(16));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), null, 3, 4);
        assertNode(bst.getRoot().getRight(), 12, 15, 17);
        assertNode(bst.getRoot().getRight().getRight(), 16, 17, null);

        bst.put(new BinaryNodeImpl<>(18));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), null, 3, 4);
        assertNode(bst.getRoot().getRight(), 12, 15, 17);
        assertNode(bst.getRoot().getRight().getRight(), 16, 17, 18);

        bst.put(new BinaryNodeImpl<>(2));
        assertNode(bst.getRoot(), 5, 10, 15);
        assertNode(bst.getRoot().getLeft(), 3, 5, null);
        assertNode(bst.getRoot().getLeft().getLeft(), 2, 3, 4);
        assertNode(bst.getRoot().getRight(), 12, 15, 17);
        assertNode(bst.getRoot().getRight().getRight(), 16, 17, 18);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putNullValue() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        bst.put(new BinaryNodeImpl<>(null));
    }

    @Test(expected = NullPointerException.class)
    public void putNull() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        bst.put(null);
    }

}
