package com.faustool.algs;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinaryNodeImplTest {

    @Test
    public void toStringTest() throws Exception {
        BinaryNode<Integer> bn = new BinaryNodeImpl<>();
        assertThat(bn.toString()).isEqualTo("");

        bn.setValue(10);
        assertThat(bn.toString()).isEqualTo("10");

        bn.setLeft(new BinaryNodeImpl<>(5));
        assertThat(bn.toString()).isEqualTo("10");

        bn.setRight(new BinaryNodeImpl<>(15));
        assertThat(bn.toString()).isEqualTo("10");
    }

}