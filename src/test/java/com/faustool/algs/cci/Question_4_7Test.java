package com.faustool.algs.cci;

import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * Created by fausto on 05/02/2017.
 */
public class Question_4_7Test {

    Question_4_7 question = new Question_4_7();

    @Test
    public void getBuildOrder() throws Exception {
        List<String> buildOrder = question.getBuildOrder(
                new String[]{"a", "b", "c", "d", "e", "f"},
                new String[]{"a", "d", "f", "b", "b", "d", "f", "a", "d", "c"});

        assertThat(buildOrder).containsExactly("e", "f", "b", "a", "d", "c");
    }

    @Test
    public void getBuildOrderAnotherDependencyOrder() throws Exception {
        List<String> buildOrder = question.getBuildOrder(
                new String[]{"a", "b", "c", "d", "e", "f"},
                new String[]{"f", "b", "a", "d", "f", "a", "b", "d", "d", "c"});

        assertThat(buildOrder).containsExactly("e", "f", "b", "a", "d", "c");
    }


}