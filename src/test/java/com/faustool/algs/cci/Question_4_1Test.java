package com.faustool.algs.cci;

import com.faustool.algs.cci.Question_4_1.Node;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 28/01/2017.
 */
public class Question_4_1Test {
    @Test
    public void isThereARoute() throws Exception {
        Question_4_1 question = new Question_4_1();

        Node a = node("A");
        Node b = node("B");
        Node c = node("C");
        Node d = node("D");
        Node e = node("E");
        Node f = node("F");

        question.connect(a, b);
        assertDirectedRoute(question, a, b);

        question.connect(b, c);
        assertDirectedRoute(question, b, c);

        question.connect(c, d);
        assertDirectedRoute(question, c, d);

        assertDirectedRoute(question, a, d);

        question.connect(e, f);
        assertDirectedRoute(question, e, f);


        assertNoRoute(question, a, e);
        assertNoRoute(question, a, f);
        assertNoRoute(question, b, e);
        assertNoRoute(question, b, f);
        assertNoRoute(question, c, e);
        assertNoRoute(question, c, f);
        assertNoRoute(question, d, e);
        assertNoRoute(question, d, f);
    }

    private void assertNoRoute(Question_4_1 question, Node from, Node to) {
        assertThat(question.isThereARoute(from, to)).isFalse();
        assertThat(question.isThereARoute(to, from)).isFalse();
    }

    private void assertDirectedRoute(Question_4_1 question, Node from, Node to) {
        assertThat(question.isThereARoute(from, to)).isTrue();
        assertThat(question.isThereARoute(to, from)).isFalse();
    }

    private Node node(String name) {
        return new Node(name);
    }

}