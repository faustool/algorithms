package com.faustool.algs.cci;

import com.faustool.algs.cci.Question_4_3.Node;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_3Test {

    @Test
    public void test() {
        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4);
        Node _5 = new Node(5);
        Node _6 = new Node(6);
        Node _7 = new Node(7);
        Node _8 = new Node(8);

        _4.left = _2;
        _4.right = _6;

        _2.left = _1;
        _2.right = _3;

        _6.left = _5;
        _6.right = _7;

        _7.right = _8;

        List<List<Node>> depthLists = new Question_4_3().getDepthLists(_4);

        assertThat(depthLists).hasSize(4);
        assertThat(depthLists.get(0)).containsExactly(_4);
        assertThat(depthLists.get(1)).containsExactly(_2, _6);
        assertThat(depthLists.get(2)).containsExactly(_1, _3, _5, _7);
        assertThat(depthLists.get(3)).containsExactly(_8);
    }

}