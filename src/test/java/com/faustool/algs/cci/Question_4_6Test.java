package com.faustool.algs.cci;

import com.faustool.algs.cci.Question_4_6.Node;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_6Test {

    private final Question_4_6 question = new Question_4_6();
    private Node[] nodes;

    @Test
    public void getNextInOrder() throws Exception {
        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4);
        Node _5 = new Node(5);
        Node _6 = new Node(6);
        Node _7 = new Node(7);

        link(_2, _4, _6);
        link(_1, _2, _3);
        link(_5, _6, _7);

        Question_4_6 question = new Question_4_6();

        assertThat(question.getNextInOrder(_1)).isEqualTo(_2);
        assertThat(question.getNextInOrder(_2)).isEqualTo(_3);
        assertThat(question.getNextInOrder(_3)).isEqualTo(_4);
        assertThat(question.getNextInOrder(_4)).isEqualTo(_5);
        assertThat(question.getNextInOrder(_5)).isEqualTo(_6);
        assertThat(question.getNextInOrder(_6)).isEqualTo(_7);
        assertThat(question.getNextInOrder(_7)).isNull();

    }

    @Test
    public void getNextInOrderBigger() throws Exception {
        nodes = new Node[31];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node(i + 1);
        }

        link(1, 2, 3);
        link(5, 6, 7);
        link(9, 10, 11);
        link(13, 14, 15);
        link(17, 18, 19);
        link(21, 22, 23);
        link(25, 26, 27);
        link(29, 30, 31);

        link(2, 4, 6);
        link(10, 12, 14);
        link(18, 20, 22);
        link(26, 28, 30);

        link(4, 8, 12);
        link(20, 24, 28);

        link(8, 16, 24);

        for (int i = 0; i < nodes.length; i++) {
            assertNext(i, i + 1);
        }
    }

    @Test
    public void getNextInOrderEdgeCases() throws Exception {
        nodes = new Node[31];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node(i + 1);
        }

        link(1, 2, 3);
        //link(5, 6, 7, nodes);
        //link(9, 10, 11, nodes);
        //link(13, 14, 15, nodes);
        //link(17, 18, 19, nodes);
        link(21, 22, 23);
        link(null, 26, 27);
        //link(29, 30, 31, nodes);

        link(2, 4, 6);
        //link(10, 12, 14, nodes);
        link(null, 20, 22);
        link(26, 28, null);

        link(4, 8, null);
        link(20, 24, 28);

        link(8, 16, 24);

        assertNextNotInLoop(1, 2);
        assertNextNotInLoop(2, 3);
        assertNextNotInLoop(3, 4);
        assertNextNotInLoop(4, 6);
        assertNextNotInLoop(6, 8);
        assertNextNotInLoop(8, 16);
        assertNextNotInLoop(16, 20);
        assertNextNotInLoop(20, 21);
        assertNextNotInLoop(21, 22);
        assertNextNotInLoop(22, 23);
        assertNextNotInLoop(23, 24);
        assertNextNotInLoop(24, 26);
        assertNextNotInLoop(26, 27);
        assertNextNotInLoop(27, 28);
        assertNextNotInLoop(28, null);
    }

    private void assertNextNotInLoop(int first, Integer second) {
        if (second != null)
            assertNext(first - 1, second - 1);
        else
            assertNext(first - 1, null);
    }

    private void assertNext(int first, Integer second) {
        if (second != null && second < nodes.length)
            assertThat(question.getNextInOrder(nodes[first])).isEqualTo(nodes[second]);
        else
            assertThat(question.getNextInOrder(nodes[first])).isNull();
    }


    private void link(Integer left, Integer center, Integer right) {
        link(node(left), node(center), node(right));
    }

    private Node node(Integer index) {
        if (index != null)
            return nodes[index - 1];
        else
            return null;
    }

    private void link(Node left, Node center, Node right) {
        center.left = left;
        center.right = right;
        if (left != null)
            left.parent = center;
        if (right != null)
            right.parent = center;
    }

}