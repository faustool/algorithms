package com.faustool.algs.cci;

import org.junit.Test;

import static com.faustool.algs.cci.Question_4_4.Node;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_4Test {
    @Test
    public void isBalanced() throws Exception {
        Question_4_4 question = new Question_4_4();

        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4);
        Node _5 = new Node(5);
        Node _6 = new Node(6);
        Node _7 = new Node(7);

        _4.left = _2;
        _4.right = _6;

        _2.left = _1;
        _2.right = _3;

        _6.left = _5;
        _6.right = _7;

        assertThat(question.isBalanced(_4)).isTrue();
    }

    @Test
    public void isNotBalanced() throws Exception {
        Question_4_4 question = new Question_4_4();

        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4); // root
        Node _6 = new Node(6);
        Node _7 = new Node(7);
        Node _8 = new Node(8);
        Node _9 = new Node(9);
        Node _10 = new Node(10);

        _4.left = _2;
        _4.right = _9;

        _2.left = _1;
        _2.right = _3;

        _9.left = _8;
        _9.right = _10;

        _8.left = _7;
        _7.left = _6;

        assertThat(question.isBalanced(_4)).isFalse();
    }

}