package com.faustool.algs.cci;

import com.faustool.algs.cci.Question_4_2.Node;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 28/01/2017.
 */
public class Question_4_2Test {
    @Test
    public void createTree0_6() throws Exception {
        Question_4_2 question = new Question_4_2();

        Node tree = question.createTree(0, 1, 2, 3, 4, 5, 6);

        assertNode(tree, 1, 3, 5);
        assertNode(tree.left, 0, 1, 2);
        assertNode(tree.right, 4, 5, 6);
    }

    @Test
    public void createTree0_7() throws Exception {
        Question_4_2 question = new Question_4_2();

        Node tree = question.createTree(0, 1, 2, 3, 4, 5, 6, 7);

        assertNode(tree, 2, 4, 6);
        assertNode(tree.left, 1, 2, 3);
        assertNode(tree.left.left, 0, 1, null);
        assertNode(tree.left.left.left, null, 0, null);
        assertNode(tree.left.right, null, 3, null);

        assertNode(tree.right, 5, 6, 7);
        assertNode(tree.right.left, null, 5, null);
        assertNode(tree.right.right, null, 7, null);
    }

    private void assertNode(Node tree, Integer left, Integer center, Integer right) {
        if (center != null) {
            assertThat(tree.value).isEqualTo(center);
            if (left != null)
                assertThat(tree.left.value).isEqualTo(left);
            else
                assertThat(tree.left).isNull();
            if (right != null)
                assertThat(tree.right.value).isEqualTo(right);
            else
                assertThat(tree.right).isNull();
        } else {
            assertThat(tree).isNull();
        }

    }

}