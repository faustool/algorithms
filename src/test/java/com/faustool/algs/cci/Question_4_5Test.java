package com.faustool.algs.cci;

import org.junit.Test;

import static com.faustool.algs.cci.Question_4_5.*;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 04/02/2017.
 */
public class Question_4_5Test {
    @Test
    public void validateBinarySearchTreeTrue() throws Exception {
        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4);
        Node _5 = new Node(5);
        Node _6 = new Node(6);
        Node _7 = new Node(7);

        _4.left = _2;
        _4.right = _6;

        _2.left = _1;
        _2.right = _3;

        _6.left = _5;
        _6.right = _7;

        Question_4_5 question = new Question_4_5();
        assertThat(question.isBinarySearchTree(_4)).isTrue();
    }

    @Test
    public void validateBinarySearchTreeFalse() throws Exception {
        int i = 1;
        Node _1 = new Node(1);
        Node _2 = new Node(2);
        Node _3 = new Node(3);
        Node _4 = new Node(4);
        Node _5 = new Node(5);
        Node _6 = new Node(6);
        Node _7 = new Node(7);
        Node _8 = new Node(8);
        Node _9 = new Node(9);
        Node __9 = new Node(9);
        Node _10 = new Node(10);
        Node _12 = new Node(12);
        Node _13 = new Node(13);

        link(_4, _6, _10);
        link(_2, _4, _5);
        link(_1, _2, _3);
        link(_8, _10, _12);
        link(_7, _8, _9);
        link(__9, _12, _13); // this second 9 at left of 12 is the faulty guy

        Question_4_5 question = new Question_4_5();
        assertThat(question.isBinarySearchTree(_6)).isFalse();
    }

    private void link(Node left, Node center, Node right) {
        center.left = left;
        center.right = right;
    }
}