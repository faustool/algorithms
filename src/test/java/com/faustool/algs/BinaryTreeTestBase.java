package com.faustool.algs;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinaryTreeTestBase {
    public void assertNode(BinaryNode<Integer> node, Integer left, Integer it, Integer right) {
        if (it == null) {
            assertThat(node).isNull();
            return;
        } else {
            assertThat(node).isNotNull();
            assertThat(node.getValue()).isEqualTo(it);
        }
        if (left == null) {
            assertThat(node.getLeft()).isNull();
        } else {
            assertThat(node.getLeft()).isNotNull();
            assertThat(node.getLeft().getValue()).isEqualTo(left);
        }

        if (right == null) {
            assertThat(node.getRight()).isNull();
        } else {
            assertThat(node.getRight()).isNotNull();
            assertThat(node.getRight().getValue()).isEqualTo(right);
        }
    }
}
