package com.faustool.algs;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by fausto on 22/01/2017.
 */
public class BinaryNodeVisitorTest {
    @Test
    public void visitOrders() {
        CompleteBinaryTree<String> bt = new CompleteBinaryTree<>();

        final List<String> visits = new ArrayList<>();
        Consumer<String> visitor = visits::add;

        assertThat(visits).isEmpty();

        bt.put(new BinaryNodeImpl<>("root"));
        bt.put(new BinaryNodeImpl<>("left"));
        bt.put(new BinaryNodeImpl<>("right"));

        visits.clear();
        new InOrderBinaryNodeVisitor<String>().visit(bt.getRoot(), visitor);
        assertThat(visits).containsExactly("left", "root", "right");

        visits.clear();
        new PreOrderBinaryNodeVisitor<String>().visit(bt.getRoot(), visitor);
        assertThat(visits).containsExactly("root", "left", "right");

        visits.clear();
        new PostOrderBinaryNodeVisitor<String>().visit(bt.getRoot(), visitor);
        assertThat(visits).containsExactly("left", "right", "root");
    }
}
